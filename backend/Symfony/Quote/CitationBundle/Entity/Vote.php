<?php

namespace Quote\CitationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vote
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Quote\CitationBundle\Entity\VoteRepository")
 */
class Vote
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=100)
     */
    private $ip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datevote", type="datetime")
     */
    private $datevote;

    /**
     * @var integer
     *
     * @ORM\Column(name="citation_id", type="integer")
     */
    private $citationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="value_vote", type="integer")
     */
    private $valueVote;

    /**
     * @var integer
     *
     * @ORM\Column(name="userid", type="integer")
     */
    private $userid;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Vote
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set datevote
     *
     * @param \DateTime $datevote
     * @return Vote
     */
    public function setDatevote($datevote)
    {
        $this->datevote = $datevote;

        return $this;
    }

    /**
     * Get datevote
     *
     * @return \DateTime 
     */
    public function getDatevote()
    {
        return $this->datevote;
    }

    /**
     * Set citationId
     *
     * @param integer $citationId
     * @return Vote
     */
    public function setCitationId($citationId)
    {
        $this->citationId = $citationId;

        return $this;
    }

    /**
     * Get citationId
     *
     * @return integer 
     */
    public function getCitationId()
    {
        return $this->citationId;
    }

    /**
     * Set valueVote
     *
     * @param integer $valueVote
     * @return Vote
     */
    public function setValueVote($valueVote)
    {
        $this->valueVote = $valueVote;

        return $this;
    }

    /**
     * Get valueVote
     *
     * @return integer 
     */
    public function getValueVote()
    {
        return $this->valueVote;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return Vote
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }
}
