<?php

namespace Quote\CitationBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Citation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Quote\CitationBundle\Entity\CitationRepository")
 * @UniqueEntity(fields="citation", message="Cette citation existe déjà.")
 */

class Citation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_image", type="string", length=100)
     */
    private $nom_image;

    /**
     * @var string
     *
     * @ORM\Column(name="auteur", type="string", length=100)
     * @Assert\Length(
     *      min = "2",
     *      max = "25",
     *      minMessage = "L'auteur' doit faire au moins {{ limit }} caractères.",
     *      maxMessage = "L'auteur' ne doit pas dépasser {{ limit }} caractères."
     * )
     */
    private $auteur;

    /**
     * @var integer
     *
     * @ORM\Column(name="user", type="integer")
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score;

    /**
     * @var string
     *
     * @ORM\Column(name="citation", type="text")
     * @Assert\Length(
     *      min = "7",
     *      minMessage = "La citation doit faire au moins {{ limit }} caractères."
     * )
     */
    private $citation;

    /**
     * @var string
     *
     * @ORM\Column(name="url_image_base", type="string", length=255)
     */
    private $url_image_base;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_upload", type="datetime")
     * @Assert\DateTime()
     */
    private $date_upload;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255)
     * @Assert\Length(
     *      max = "50",
     *      maxMessage="Le genre ne doit pas dépasser {{ limit }} caractères."
     * )
     *
     * @Assert\Regex(
     *     pattern="/genre/i",
     *     match=false,
     *     message="Genre invalide"
     * )
     */
    private $tag;

    /**
     * @var integer
     *
     * @ORM\Column(name="langue", type="integer")
     */
    private $langue;

    private $attachment;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom_image
     *
     * @param string $nomImage
     * @return Citation
     */
    public function setNomImage($nomImage)
    {
        $this->nom_image = $nomImage;
    
        return $this;
    }

    /**
     * Get nom_image
     *
     * @return string 
     */
    public function getNomImage()
    {
        return $this->nom_image;
    }

    /**
     * Set auteur
     *
     * @param string $auteur
     * @return Citation
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;
    
        return $this;
    }

    /**
     * Get auteur
     *
     * @return string 
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set citation
     *
     * @param string $citation
     * @return Citation
     */
    public function setCitation($citation)
    {
        $this->citation = $citation;
    
        return $this;
    }

    /**
     * Get citation
     *
     * @return string 
     */
    public function getCitation()
    {
        return $this->citation;
    }

    /**
     * Set url_image_base
     *
     * @param string $urlImageBase
     * @return Citation
     */
    public function setUrlImageBase($urlImageBase)
    {
        $this->url_image_base = $urlImageBase;
    
        return $this;
    }

    /**
     * Get url_image_base
     *
     * @return string 
     */
    public function getUrlImageBase()
    {
        return $this->url_image_base;
    }

    /**
     * Set date_upload
     *
     * @param \DateTime $dateUpload
     * @return Citation
     */
    public function setDateUpload($dateUpload)
    {
        $this->date_upload = $dateUpload;
    
        return $this;
    }

    /**
     * Get date_upload
     *
     * @return \DateTime 
     */
    public function getDateUpload()
    {
        return $this->date_upload;
    }


    /**
     * Set tag
     *
     * @param string $tag
     * @return Citation
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    
        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }




    /**
     * Set user
     *
     * @param integer $user
     * @return Citation
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }



        /**
     * Set type
     *
     * @param integer $type
     * @return Citation
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }



     /**
     * Set score
     *
     * @param integer $score
     * @return Citation
     */
    public function setScore($score)
    {
        $this->score = $score;
    
        return $this;
    }

    /**
     * Get score
     *
     * @return integer 
     */
    public function getScore()
    {
        return $this->score;
    }

     /**
     * Set langue
     *
     * @param integer $langue
     * @return Citation
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;
    
        return $this;
    }

    /**
     * Get langue
     *
     * @return integer 
     */
    public function getLangue()
    {
        return $this->langue;
    }

    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
    
        return $this;
    }
    public function getAttachment()
    {
        return $this->attachment;
    }
}
