<?php

namespace Quote\CitationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Quote\CitationBundle\Entity\UserRepository")
 * @UniqueEntity(fields="pseudo", message="Ce nom d'utilisateur existe déjà, veuillez en choisir un autre.")
 * @UniqueEntity(fields="email", message="Cette adresse email existe déjà, veuillez en choisir une autre.")
 */
class User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pseudo", type="string", length=50)
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9-]+$/",
     *     match=true,
     *     message="Votre nom d'utilisateur doit etre composé seulement de lettres et de chiffres"
     * )
     * @Assert\Length(
     *      min = "3",
     *      max = "25",
     *      minMessage = "Votre nom d'utilisateur doit faire au moins {{ limit }} caractères.",
     *      maxMessage = "Votre nom d'utilisateur ne doit pas dépasser {{ limit }} caractères."
     * )
     */
    private $pseudo;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150)
     * @Assert\Email(
     *     message = "'{{ value }}' n'est pas un email valide.",
     *     checkMX = true
     * )
     * @Assert\Length(
     *      min = "5",
     *      max = "150",
     *      minMessage = "Votre adresse email doit faire au moins {{ limit }} caractères.",
     *      maxMessage = "Votre adresse email ne doit pas dépasser {{ limit }} caractères."
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="text")
     * @Assert\Length(
     *      min = "6",
     *      minMessage = "Votre mot de passe doit faire au moins {{ limit }} caractères."
     * )
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last", type="datetime")
     */
    private $last;

    /**
     * @var integer
     *
     * @ORM\Column(name="visite", type="integer")
     */
    private $visite;

    /**
     * @var integer
     *
     * @ORM\Column(name="admin", type="integer")
     */
    private $admin;

    public function __construct()
    {
        $this->last = new \Datetime();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pseudo
     *
     * @param string $pseudo
     * @return User
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * Get pseudo
     *
     * @return string 
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return User
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set last
     *
     * @param \DateTime $last
     * @return User
     */
    public function setLast($last)
    {
        $this->last = $last;

        return $this;
    }

    /**
     * Get last
     *
     * @return \DateTime 
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * Set visite
     *
     * @param integer $visite
     * @return User
     */
    public function setVisite($visite)
    {
        $this->visite = $visite;

        return $this;
    }

    /**
     * Get visite
     *
     * @return integer 
     */
    public function getVisite()
    {
        return $this->visite;
    }


    /**
     * Set admin
     *
     * @param integer $admin
     * @return User
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return integer 
     */
    public function getAdmin()
    {
        return $this->admin;
    }
}
