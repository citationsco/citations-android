<?php

namespace Quote\CitationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CitationType extends AbstractType
{
    private $genre;
    private $type;

    public function __construct($genre, $type)
    {
        $this->genre = $genre;
        $this->type = $type;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('attachment', 'file',
               array(
                        'required'  => false                       
                    ))
            ->add('auteur')
            ->add('citation')
            ->add('type', 'choice', array(
            'choices' => $this->type,
            ))
            ->add('tag', 'choice', array(
            'choices' => $this->genre,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Quote\CitationBundle\Entity\Citation'
        ));
    }

    public function getName()
    {
        return 'quote_citationbundle_citationtype';
    }
}
