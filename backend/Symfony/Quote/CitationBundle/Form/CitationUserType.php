<?php

namespace Quote\CitationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CitationUserType extends AbstractType
{
    public function __construct()
    {

    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pseudo', 'text')
            ->add('email', 'email')
        ;
    $builder->add('password', 'repeated', array(
        'type' => 'password',
        'invalid_message' => 'Les mots de passe doivent correspondre',
        'options' => array('required' => true),
        'first_options'  => array('label' => 'Mot de passe'),
        'second_options' => array('label' => 'Mot de passe (validation)')
    ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Quote\CitationBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'quote_citationbundle_citationusertype';
    }
}
