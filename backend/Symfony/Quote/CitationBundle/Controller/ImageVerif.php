<?php
namespace Quote\CitationBundle\Controller;

class ImageVerif
{
	protected $image;
	protected $info_image;
	protected $largeur;
	protected $hauteur;
	protected $size;
	protected $mime;
	protected $error;
	
	public function __construct($image,$image_size,$info_image)
	{
		//-------------------------------------------------
		$this->image = $image;
		$this->size = $image_size;
		$this->mime = $info_image['mime'];
		$this->largeur = $info_image['0'];
		$this->hauteur = $info_image['1'];
	}
	public function random_name()
	{
	    return rand(1,1000) * rand(1,1000) * rand(1,100); // 1 / 1 milliard
	}
	public function verif_image()
	{
		$this->verif_size();
		$this->verif_ext();
		$this->verif_dimension();
		$error = $this->error;
		if(empty($error))
		{
			return null;
		}
		else
		{
			return substr($error,6);
		}
		
	}
    public function verif_size()
	{
    	if($this->size > 2097152)
    	{
    		$this->error = $this->error."<br />Votre image est trop volumineuse, elle ne doit pas dépasser 2MO.";
    	}
		
	}
    public function verif_ext()
	{
	    if($this->mime != "image/jpeg" && $this->mime != "image/png" && $this->mime != "image/jpg")
	    {
	    	$this->error = $this->error."<br />Format d'image incorrect. Les types d'images autorisés sont jpg/jpeg et png.";
	    }
	}
    public function verif_dimension()
	{
    	if($this->largeur < 400 || $this->hauteur < 400)
    	{
    		$this->error = $this->error."<br />Les dimensions de votre image sont trop petites. Elles doivent avoir minimum 400 pixels en hauteur et en largeur.";
    	}
	}
}