<?php
namespace Quote\CitationBundle\Controller;

use Quote\CitationBundle\Entity;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Quote\CitationBundle\Entity\Citation;
use Quote\CitationBundle\Entity\User;
use Quote\CitationBundle\Entity\Vote;
use Quote\CitationBundle\Entity\Comment;
use Quote\CitationBundle\Form\CitationType;
use Quote\CitationBundle\Form\CitationUserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Quote\CitationBundle\Controller\ImageVerif;
use Quote\CitationBundle\Controller\ImageTraitement;
use Quote\CitationBundle\Controller\ImageTraitement2;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\Query\ResultSetMapping;

class CitationController extends Controller
{
    const SCORE = 4;
    const LIMITEMESSAGE = 5;
    const LIMITEMESSAGEBOX = 15;
    public $LIMITESCROLL = 8;


public function __construct()
{

}

//---------------------------------------------------------------------------------------- Mobile
public function mobile()
{
    $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    if (preg_match ( "/phone|iphone|itouch|ipod|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/",$user_agent))
        return true;
}

public function android()
{
    $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
    if(stripos($ua,'android') !== false) 
        return true;
    return false;
}

public function androidAction(Request $request)
{
    $session = $request->getSession();
    $session->set('android', true);
    return $this->render('QuoteCitationBundle:Citation:android.html.twig');
}

//---------------------------------------------------------------------------------------- DeleteCitation
    public function deleteAction(Request $request, $id)
    {
        $session = $request->getSession();
        if ($session->get('user_id'))
        {
            $cit = $this->get_info_citation($id);
            if ($cit && ($cit->getUser() == $session->get('user_id')) || $session->get('admin') == 1)
            {
                $em = $this->getDoctrine()->getManager();
                $em->remove($cit);
                $em->flush();
                return $this->redirect($this->getRequest()->headers->get('referer'));
            }
            return $this->redirect($this->generateUrl('quote_citation_index'));
        }
        return $this->redirect($this->generateUrl('quote_citation_index'));
    }
//---------------------------------------------------------------------------------------- Commentaire
    public function nbr_comment_citation($id_user, $id_citation, $admin)
    {
        $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
            'SELECT c
            FROM QuoteCitationBundle:Comment c
            WHERE (c.idCitation = :id_citation AND c.idUser = :user_id)')
            ->setParameters(array('id_citation'=> $id_citation, 'user_id'=> $id_user));
        $result = $query->getResult();
        if (count($result) >= self::LIMITEMESSAGE AND $admin == 0)
            return false;
        return true;
    }

    public function get_message($id_citation)
    {
        $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
            'SELECT c
            FROM QuoteCitationBundle:Comment c
            WHERE (c.idCitation = :id_citation) ORDER BY c.dateAdd DESC')
            ->setParameters(array('id_citation'=> $id_citation))
            ->setFirstResult(0)
            ->setMaxResults(self::LIMITEMESSAGEBOX);
        $result = $query->getResult();
        return ($result);
    }

    public function addmessageAction(Request $request)
    {
        $session = $request->getSession();
        $request = $this->get('request');
        $error = "";
        if ($request->getMethod() == 'POST' && isset($_POST['citation_id']) && $this->citation_exist($_POST['citation_id']) && $session->get('user_id') && isset($_POST['Ajouter']) && isset($_POST['comment']))
        {
            if (strlen($_POST['comment']) > 200 || strlen($_POST['comment']) < 2)
                $error = "<p class='name' style='margin-left: 10px;color: #b8b7b7;font-size: 18px;text-align:center;'>Votre commentaire doit être compris entre 2 et 200 caracteres.</p>";
            else
            {
                if ($this->nbr_comment_citation($session->get('user_id'), $_POST['citation_id'], $session->get('admin')))
                {
                    $comment = new Comment();
                    $comment->setIdUser($session->get('user_id'));
                    $comment->setIdCitation($_POST['citation_id']);
                    $comment->setCommentaire($_POST['comment']);
                    $comment->setDateAdd(new \Datetime());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($comment);
                    $em->flush();
                    $url = $this->generateUrl('quote_citation_user_profile', array('id' => $session->get('user_id')));
                    $pseudo = $this->get_info_user($session->get('user_id'))->getPseudo();
                    $error = '<p id="message"><strong><a href="'.$url.'" title="Allez sur le profile de '.$pseudo.'">'.$pseudo.'</a></strong><br />'.$_POST['comment'].'</p>';
                }
                else
                   $error = "<p class='name' style='text-align:center;margin-left: 10px;color: #b8b7b7;font-size: 18px;'>Vous ne pouvez plus ajouter de commentaires pour cette citation.</p>"; 
            }
        }
       return new Response($error);
    }

//---------------------------------------------------------------------------------------- Menuleft
    public function leftmenuAction(Request $request, $id)
    {
        $exist = $logged = 0;
        $count_message = 1;
        $error = $tag = "";
        $session = $request->getSession();
        if ($session->get('user_id'))
            $logged = 1;
        if ($this->citation_exist($id))
        {
            $exist = 1;
            $cit = $this->get_info_citation($id);
            $message = $this->get_message($id);
            $mess = array();
            if (count($message) == 0)
            {
                $count_message = 0;
            }
            foreach ($message as $key => $value) 
            {
                $mess[] = array('comment' => $value->getCommentaire(),
                    'date' => $value->getDateAdd(), 
                    'pseudo' => $this->get_info_user($value->getIdUser())->getPseudo());
            }
            $citation = array(
                  'id' => $id,
                  'auteur' => $cit->getAuteur(),
                  'imagename' => $cit->getNomImage(),
                  'citation' => str_replace("+", " ", $cit->getCitation()),
                  'tag' => $cit->getTag(),
                  'score' => $cit->getScore(),
                  'id_user' => $cit->getUser()
                );
                if ($cit->getUser() == 0)
                    $citation['pseudo'] = "Admin";
                else
                    $citation['pseudo'] = $this->get_info_user($cit->getUser())->getPseudo();
            if ($citation['score'] > 0)
                $citation['score'] = "+".$citation['score'];
        }
        return $this->render('QuoteCitationBundle:Citation:menuleft.html.twig', array('citation' => $citation, 'exist' => $exist, 'logged' =>$logged, 'error' => $error, 'message' => $mess, 'count_message' => $count_message
              ));
    }

//---------------------------------------------------------------------------------------- CitationInfo
    public function get_info_citation($id)
    {
        $citation = $this->getDoctrine()->getRepository('QuoteCitationBundle:Citation')->find($id);
        return ($citation);
    }
//---------------------------------------------------------------------------------------- Profil !

    public function user_exist($id)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT u.id
            FROM QuoteCitationBundle:User u
            WHERE u.id = :id'
        )->setParameters(array('id'=> $id));
        try {
            $result = $query->getSingleResult();
            return (true);
        }
        catch (\Doctrine\Orm\NoResultException $e){
            return (false);
        }
    }

    public function get_citation_user($id)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT DISTINCT u
            FROM QuoteCitationBundle:Citation u
            WHERE u.user = :id ORDER BY u.id DESC'
        )->setParameters(array('id'=> $id));
        $result = $query->getResult();
        $citation = array();
        foreach ($result as $value) 
        {
            if (file_exists($this->container->getParameter('kernel.root_dir').'/../web/citations-upload/'.$value->getNomImage().'.png'))
            {
                if ($value->getUser() == 0)
                    $pseudo = "Admin";
                else
                    $pseudo = $this->get_info_user($value->getUser())->getPseudo();
                $citation[] = array(
                    'id' => $value->getId(),
                    'auteur' => $value->getAuteur(), 
                    'imagename' => $value->getNomImage(), 
                    'citation' => strtr($value->getCitation(),array(" " =>"+","%" =>"","." =>"","," =>"",":" =>"","'" =>"")),
                    'pseudo' => $pseudo,
                    'id_user' => $value->getUser(),
                    'score' => $value->getScore()
                    );
            }
        }
        return $citation;
    }

    public function get_citation_like($id)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT DISTINCT u.citationId
            FROM QuoteCitationBundle:Vote u
            WHERE u.userid = :id AND u.userid != 0 AND u.valueVote = 1 ORDER BY u.datevote DESC'
        )->setParameters(array('id'=> $id));
        $result = $query->getResult();
        $cit = array();
        foreach ($result as $value) 
        {
            $citation = $this->get_info_citation($value['citationId']);
            if ($citation && file_exists($this->container->getParameter('kernel.root_dir').'/../web/citations-upload/'.$citation->getNomImage().'.png'))
            {
                if ($citation->getUser() == 0)
                    $pseudo = "Admin";
                else
                    $pseudo = $this->get_info_user($citation->getUser())->getPseudo();
                $cit[] = array(
                    'id' => $citation->getId(),
                    'auteur' => $citation->getAuteur(), 
                    'imagename' => $citation->getNomImage(), 
                    'citation' => strtr($citation->getCitation(),array(" " =>"+","%" =>"","." =>"","," =>"",":" =>"","'" =>"")),
                    'pseudo' => $pseudo,
                    'id_user' => $citation->getUser(),
                    'score' => $citation->getScore()
                    );
            }
        }
        return $cit;
    }

    public function user_profileAction(Request $request, $id)
    {
        $session = $request->getSession();
        // if ($session->get('user_id'))
        // {
            if ($this->user_exist($id) || $id == 0)
            {
                if ($id != 0)
                    $pseudo = $this->get_info_user($id)->getPseudo();
                else
                    $pseudo = "Admin";
                $citation_user = $this->get_citation_user($id);
                $citation_like = $this->get_citation_like($id);
                return $this->render('QuoteCitationBundle:Citation:profile.html.twig', array('pseudo' => $pseudo, 'citation_user' => $citation_user, 'citation_like' => $citation_like));
            }
            return $this->redirect($this->generateUrl('quote_citation_index'));
        // }
        // return new Response("Vous devez être connecté pour voir des profils.");
    }

    public function get_info_user($id)
    {
        $user = $this->getDoctrine()->getRepository('QuoteCitationBundle:User')->find($id);
        return ($user);
    }

    //---------------------------------------------------------------------------------------- Vote

    public function dejavoter($id, $admin)
    {
        if ($admin == 1)
            return false;
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT u.id
            FROM QuoteCitationBundle:Vote u
            WHERE u.ip = :ip AND u.citationId = :id'
        )->setParameters(array('ip'=> $_SERVER['REMOTE_ADDR'], 'id'=> $id));
        try {
            $result = $query->getSingleResult();
            return (true);
        }
        catch (\Doctrine\Orm\NoResultException $e){
            return (false);
        }
    }

    public function user_score_updateAction(Request $request, $score, $id)
    {
        $session = $request->getSession();
        $admin = 0;
        if ($score == 1 || $score == 0 || $score == 2)
        {
            if ($score == 0)
                $score = -1;
            if ($score == 2)
                $score = 0;
            if($citation = $this->get_info_citation($id))
            {
                if ($session->get('admin') && $session->get('admin') == 1)
                    $admin = 1;
                if (!$this->dejavoter($id, $admin) && $score != 0)
                {
                    $vote = new Vote();
                    $vote->setIp($_SERVER['REMOTE_ADDR']);
                    $vote->setDatevote(new \Datetime());
                    $vote->setCitationId($id);
                    $vote->setValueVote($score);
                    if ($session->get('user_id'))
                        $vote->setUserid($session->get('user_id'));
                    else
                        $vote->setUserid(0);
                    $citation->setScore($citation->getScore() + $score);
                    if ($citation->getScore() == self::SCORE)
                        $citation->setDateUpload(new \Datetime());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($citation);
                    $em->persist($vote);
                    $em->flush();
                }
            }
        }
        return new Response(($citation->getScore() > 0) ?  "+".$citation->getScore() : $citation->getScore());
    }
//----------------------------------------------------------------------------------------Inscription, connexion ,deconnexion
    public function inscriptionAction(Request $request)
    {
        $session = $request->getSession();
        if (!$session->get('user_id'))
        {
            $message_valide = "";
            $user = new User();
            $form = $this->createForm(new CitationUserType($this->builtGenreFormAction()), $user);
            $request = $this->get('request');
            if( $request->getMethod() == 'POST')
            {
                $info_inscription = $_POST['quote_citationbundle_citationusertype'];
                $form->bind($request);
                if( $form->isValid())
                {
                    $user->setPseudo($info_inscription['pseudo']);
                    $user->setEmail($info_inscription['email']);
                    $user->setPassword(hash("whirlpool", $info_inscription['password']['first']));
                    $user->setDate(new \Datetime());
                    $user->setVisite(0);
                    $user->setAdmin(0);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    $message_valide = "Vous êtes maintenant inscris !<br /><a class='simplelink' href='".$this->generateUrl('quote_citation_connexion')."'>Se connecter</a>"; 
                }
            }
            return $this->render('QuoteCitationBundle:Citation:inscription.html.twig', array(
                'form' => $form->createView(),'message_valide' =>$message_valide
              ));
        }
        return $this->redirect($this->generateUrl('quote_citation_index'));
    }

    public function connexionAction(Request $request)
    {
        $session = $request->getSession();
        if (!$session->get('user_id'))
        {
            $message_valide = $error = "";
            $request = $this->get('request');
            if( $request->getMethod() == 'POST' && isset($_POST['connexion']) && isset($_POST['nom']) && isset($_POST['password']))
            {
                $em = $this->getDoctrine()->getManager();
                $query = $em->createQuery(
                    'SELECT u.id
                    FROM QuoteCitationBundle:User u
                    WHERE (u.pseudo = :pseudo OR u.email = :email) AND u.password = :password
                    ORDER BY u.id ASC'
                )->setParameters(array('pseudo'=> $_POST['nom'],'email'=> $_POST['nom'], 'password'=> hash("whirlpool", $_POST['password'])));
                try {
                    $result = $query->getSingleResult();
                    $user = $this->get_info_user($result['id']);
                    $message_valide = "Connexion reussis !";
                    $session->set('user_id', $result['id']);
                    $session->set('admin', $user->getAdmin());
                    $user->setVisite($user->getVisite() + 1);
                    $user->setLast(new \Datetime());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    return $this->redirect($this->generateUrl('quote_citation_index'));
                }
                catch (\Doctrine\Orm\NoResultException $e){
                    $result = null;
                    $error = "Nom d'utilisateur (mail) ou mot de passe incorrect";
                }
            }
            return $this->render('QuoteCitationBundle:Citation:connexion.html.twig', array('message_valide' =>$message_valide, 'error' =>$error));
        }
        return $this->redirect($this->generateUrl('quote_citation_index'));
    }

    public function deconnexionAction(Request $request)
    {
        $session = $request->getSession();
        if ($session->get('user_id'))
            $session->remove('user_id');
        if ($session->get('admin'))
            $session->remove('admin');
        return $this->redirect($this->generateUrl('quote_citation_index'));
    }

//----------------------------------------------------------------------------------------requete principale et vue

    public function reqSelectCitation($start, $limit, $type, $contrib = "")
    {
        if (empty($score))
            $score = self::SCORE;
        $em = $this->getDoctrine()->getManager();
        if (!empty($contrib))
        {
            $query = $em->createQuery(
                'SELECT c
                FROM QuoteCitationBundle:Citation c
                WHERE (c.score < :score AND c.score >= :scoremin AND c.user != 0 AND c.langue = 0)
                ORDER BY c.date_upload DESC'
            )
            ->setParameters(array('score'=> self::SCORE, 'scoremin'=> -self::SCORE))
            ->setFirstResult($start)
            ->setMaxResults($limit);
        }
        else
        {
            $query = $em->createQuery(
                'SELECT c
                FROM QuoteCitationBundle:Citation c
                WHERE c.type = :type AND (c.score >= :score OR c.user = 0) AND c.langue = 0
                ORDER BY c.date_upload DESC'
            )
            ->setParameters(array('type'=> $type, 'score'=> self::SCORE))
            ->setFirstResult($start)
            ->setMaxResults($limit);
        }
        $result = $query->getResult();
        $citation = array();
        foreach ($result as $value) 
        {
            if (file_exists($this->container->getParameter('kernel.root_dir').'/../web/citations-upload/'.$value->getNomImage().'.png'))
            {
                if ($value->getUser() == 0)
                    $pseudo = "Admin";
                else
                    $pseudo = $this->get_info_user($value->getUser())->getPseudo();
                $citation[] = array(
                    'id' => $value->getId(),
                    'auteur' => $value->getAuteur(), 
                    'imagename' => $value->getNomImage(), 
                    'citation' => strtr($value->getCitation(),array(" " =>"+","%" =>"","." =>"","," =>"",":" =>"","'" =>"")),
                    'pseudo' => $pseudo,
                    'id_user' => $value->getUser(),
                    'score' => $value->getScore()
                    );
            }
        }
        return $citation;
    }

    public function reqSelectCitationUSA($start, $limit, $type, $contrib = "")
    {
        if (empty($score))
            $score = self::SCORE;
        $em = $this->getDoctrine()->getManager();
        if (!empty($contrib))
        {
            $query = $em->createQuery(
                'SELECT c
                FROM QuoteCitationBundle:Citation c
                WHERE (c.score < :score AND c.score >= :scoremin AND c.user != 0 AND c.langue = 1)
                ORDER BY c.date_upload DESC'
            )
            ->setParameters(array('score'=> self::SCORE, 'scoremin'=> -self::SCORE))
            ->setFirstResult($start)
            ->setMaxResults($limit);
        }
        else
        {
            $query = $em->createQuery(
                'SELECT c
                FROM QuoteCitationBundle:Citation c
                WHERE c.type = :type AND (c.score >= :score OR c.user = 0) AND c.langue = 1
                ORDER BY c.date_upload DESC'
            )
            ->setParameters(array('type'=> $type, 'score'=> self::SCORE))
            ->setFirstResult($start)
            ->setMaxResults($limit);
        }
        $result = $query->getResult();
        $citation = array();
        foreach ($result as $value) 
        {
            if (file_exists($this->container->getParameter('kernel.root_dir').'/../web/citations-upload/'.$value->getNomImage().'.png'))
            {
                if ($value->getUser() == 0)
                    $pseudo = "Admin";
                else
                    $pseudo = $this->get_info_user($value->getUser())->getPseudo();
                $citation[] = array(
                    'id' => $value->getId(),
                    'auteur' => $value->getAuteur(), 
                    'imagename' => $value->getNomImage(), 
                    'citation' => strtr($value->getCitation(),array(" " =>"+","%" =>"","." =>"","," =>"",":" =>"","'" =>"")),
                    'pseudo' => $pseudo,
                    'id_user' => $value->getUser(),
                    'score' => $value->getScore()
                    );
            }
        }
        return $citation;
    }

    public function reqSelectCitationRandom($langue)
    {
        $i = 0;
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT c
            FROM QuoteCitationBundle:Citation c
            WHERE c.langue = :langue'
        )
        ->setParameters(array('langue'=> $langue));

        $result = $query->getResult();
        $randomized = rand(0, count($result) - 1);
        $citation = array();
        foreach ($result as $value) 
        {
            if ($randomized == $i)
            {
                if (file_exists($this->container->getParameter('kernel.root_dir').'/../web/citations-upload/'.$value->getNomImage().'.png'))
                {
                    if ($value->getUser() == 0)
                        $pseudo = "Admin";
                    else
                        $pseudo = $this->get_info_user($value->getUser())->getPseudo();
                    $citation[] = array(
                        'id' => $value->getId(),
                        'auteur' => $value->getAuteur(), 
                        'imagename' => $value->getNomImage(), 
                        'citation' => strtr($value->getCitation(),array(" " =>"+","%" =>"","." =>"","," =>"",":" =>"","'" =>"")),
                        'pseudo' => $pseudo,
                        'id_user' => $value->getUser(),
                        'score' => $value->getScore()
                        );
                }
                break ;
            }
            $i++;
        }
        return $citation;
    }

    public function indexAction(Request $request)
    {
        $session = $request->getSession();
        if ($this->android() && !$session->get('android'))
            return $this->redirect($this->generateUrl('quote_citation_android'));
        $src ='citations-upload';
        $href = "";
        $table ='Citation';
        return $this->render('QuoteCitationBundle:Citation:index.html.twig',array('citation' => $this->reqSelectCitation(0, $this->LIMITESCROLL, 0),'src' => $src,'href' => $href,'table' => $table,'search' => "recherche",'title' => "Accueil Citation"));
    }

    public function contributionsAction(Request $request)
    {
        $session = $request->getSession();
        if ($this->android() && !$session->get('android'))
            return $this->redirect($this->generateUrl('quote_citation_android'));
        $src ='citations-upload';
        $href = "contributions";
        $table ='CitationContrib';
        return $this->render('QuoteCitationBundle:Citation:index.html.twig',array('citation' => $this->reqSelectCitation(0, $this->LIMITESCROLL, 0, "1", -self::SCORE),'src' => $src,'href' => $href,'table' => $table,'search' => "recherche",'title' => "Toutes les citations"));
    }
    public function usersAction(Request $request)
    {
        $session = $request->getSession();
        if ($this->android() && !$session->get('android'))
            return $this->redirect($this->generateUrl('quote_citation_android'));
        $src ='citations-upload';
        $href = "user";
        $table ='CitationUser';
        return $this->render('QuoteCitationBundle:Citation:index.html.twig',array('citation' => $this->reqSelectCitation(0, $this->LIMITESCROLL, 1),'src' => $src,'href' => $href,'table' => $table,'search' => "recherche",'title' => "Vos citations"));
    }

//----------------------------------------------------------------------------------------requete recherche et vue
    public function rechercheAction($recherche)
    {
        $src ='citations-upload';
        $table ='Citation';
        return $this->render('QuoteCitationBundle:Citation:index.html.twig',array('citation' => $this->builIndexRechercheAction('0',$table,$recherche, $this->LIMITESCROLL),'src' => $src,'href' => "",'table' => "recherche","search" => $recherche,'title' => "Recherche : $recherche"));
    }
    public function builIndexRechercheAction($start,$table,$where, $limite)
    {
        $citation = array();
        $i = 0;
        foreach ($this->indexRequeteRecherche($start,$table,$where, $limite) as $key => $value) 
        {
            if ($value['user'] == 0)
                $pseudo = "Admin";
            else
                $pseudo = $this->get_info_user($value['user'])->getPseudo();
            $citation[$i]['id'] = $value['id'];
            $citation[$i]['auteur'] = $value['auteur'];
            $citation[$i]['imagename'] = $value['nom_image'];
            $citation[$i]['citation'] = strtr($value['citation'],array(" " =>"+","%" =>"","." =>"","," =>"",":" =>"","'" =>""));
            $citation[$i]['pseudo'] = $pseudo;
            $citation[$i]['id_user'] = $value['user'];
            $citation[$i]['score'] = $value['score'];
            $i++;
        }
        return $citation;
    }

    public function indexRequeteRecherche($start,$table,$where, $limite)
    {
        $em = $this->get('doctrine')->getManager();
        $query = $em->createQuery('
                SELECT p.auteur,p.id,p.citation,p.nom_image, p.user, p.score FROM QuoteCitationBundle:Citation p
                WHERE (p.auteur LIKE :recherche OR p.tag LIKE :recherche OR p.citation LIKE :recherche)
                AND p.langue = 0 ORDER BY p.score DESC')
            ->setParameter('recherche', '%'.$where.'%')
            ->setFirstResult($start)
            ->setMaxResults($limite);
        return $query->getResult();
    }

    public function builIndexRechercheUSAAction($start,$table,$where, $limite)
    {
        $citation = array();
        $i = 0;
        foreach ($this->indexRequeteRechercheUSA($start,$table,$where, $limite) as $key => $value) 
        {
            if ($value['user'] == 0)
                $pseudo = "Admin";
            else
                $pseudo = $this->get_info_user($value['user'])->getPseudo();
            $citation[$i]['id'] = $value['id'];
            $citation[$i]['auteur'] = $value['auteur'];
            $citation[$i]['imagename'] = $value['nom_image'];
            $citation[$i]['citation'] = strtr($value['citation'],array(" " =>"+","%" =>"","." =>"","," =>"",":" =>"","'" =>""));
            $citation[$i]['pseudo'] = $pseudo;
            $citation[$i]['id_user'] = $value['user'];
            $citation[$i]['score'] = $value['score'];
            $i++;
        }
        return $citation;
    }

    public function indexRequeteRechercheUSA($start,$table,$where, $limite)
    {
        $em = $this->get('doctrine')->getManager();
        $query = $em->createQuery('
                SELECT p.auteur,p.id,p.citation,p.nom_image, p.user, p.score FROM QuoteCitationBundle:Citation p
                WHERE (p.auteur LIKE :recherche OR p.tag LIKE :recherche OR p.citation LIKE :recherche)
                AND p.langue = 1 ORDER BY p.score DESC')
            ->setParameter('recherche', '%'.$where.'%')
            ->setFirstResult($start)
            ->setMaxResults($limite);
        return $query->getResult();
    }

//----------------------------------------------------------------------------------------requete ajax et vue
    public function ajaxScrollerAction($start,$table,$search = "")
    {
        $type="";
        $contrib = '';
        if($table == "Citation") // Citation
            $type = "0";
        elseif($table == "CitationContrib")  //contribution
        {
            $type = 0;
            $contrib = 1;
        }
        elseif($table == "CitationUser") //user
            $type = "1";
        if($table != "recherche" && $search == "recherche")
            $result = $this->reqSelectCitation($start, $this->LIMITESCROLL, $type, $contrib);
        else
            $result = $this->builIndexRechercheAction($start,"Citation",$search, $this->LIMITESCROLL);
        if($result != null)
            return $this->render('QuoteCitationBundle:Citation:scroll.html.twig',array('citation' => $result,'type' => $type));
        return new Response("");
    }

//----------------------------------------------------------------------------------------page simple de citation et requete

    public function citationAction(Request $request,$id,$citation)
    {
        $session = $request->getSession();
        if ($this->android() && !$session->get('android'))
            return $this->redirect($this->generateUrl('quote_citation_android'));
        $type = "citation";
    	$citationOne = $this->getDoctrine()->getRepository('QuoteCitationBundle:Citation')->find($id);
        return $this ->traitementCitationUnique($citationOne,$id,$type);
    }

    public function citation_exist($id)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT u.id
            FROM QuoteCitationBundle:Citation u
            WHERE u.id = :id'
        )->setParameters(array('id'=> $id));
        try {
            $result = $query->getSingleResult();
            return (true);
        }
        catch (\Doctrine\Orm\NoResultException $e){
            return (false);
        }
    }

    public function traitementCitationUnique($citationOne,$id,$type)
    {
        if($citationOne != null)
        {
            $citation = array(
                  'id' => $id,
                  'auteur' => $citationOne->getAuteur(),
                  'nom_image' => $citationOne->getNomImage(),
                  'citation' => $citationOne->getCitation(),
                  'tag' => $citationOne->getTag()
                );
            return $this->render('QuoteCitationBundle:Citation:citation.html.twig',array('citation' => $citation,'type' => $type));
        }
        else
        {
            $url = $this->generateUrl("quote_citation_index");
            return $this->redirect(
                sprintf('%s#%s', $url, 'no citation')
            );
        }

    }

//----------------------------------------------------------------------------------------form et requete genre, type

    public function menuRequeteGenre()
    {
        $genreObject = $this->getDoctrine();
        return $genreObject->getRepository('QuoteCitationBundle:Genre')
        ->findBy(array(),  array('genre' => 'ASC'), null, null);
    }
    public function builtGenreAction()
    {
        $genre = array();
        $i = 0;
        foreach ($this->menuRequeteGenre() as $key => $value) 
        {
            $genre[$i]['genre'] = $value->getGenre();
            $i++;
        }
        return $genre;
    }
    public function builtGenreFormAction()
    {
        $genre = array();
        $genre['genre'] = "Sélectionner";
        foreach ($this->menuRequeteGenre() as $key => $value) 
        {
            $genre[$value->getGenre()] = ucfirst($value->getGenre());
        }
        return $genre;
    }

    public function builtTypeFormAction()
    {
        $type = array();
        $type['type'] = "Sélectionner";
        $type[0] = "Citation célèbre";
        $type[1] = "Citation personnelle";
        return $type;
    }

//----------------------------------------------------------------------------------------le header
    public function headerAction()
    {
        return $this->render('QuoteCitationBundle:Citation:menu.html.twig',array('genre' => $this->builtGenreAction()));
    }

//----------------------------------------------------------------------------------------ajouter une citation

    public function proposerAction(Request $request)
    {
        $session = $request->getSession();
        $error = "";$image_file = "";$message_valide = "";$error_connect = "";
        $DestinationFile = $this->container->getParameter('kernel.root_dir').'/../web/citations-upload';
        $DestinationMobile = $this->container->getParameter('kernel.root_dir').'/../web/citations-mobile';
        $citation = new Citation();
        $form = $this->createForm(new CitationType($this->builtGenreFormAction(), $this->builtTypeFormAction()), $citation);
        $request = $this->get('request');
        if (!isset($_FILES['quote_citationbundle_citationtype']) || (isset($_FILES['quote_citationbundle_citationtype']) && empty($_FILES['quote_citationbundle_citationtype']['tmp_name']['attachment'])))
        {
            $myfiles = $this->container->getParameter('kernel.root_dir').'/../web/watermark/black.jpg';
        }
        if($request->getMethod() == 'POST' && (isset($_FILES['quote_citationbundle_citationtype']) || isset($myfiles)))
        {
            $info_cit = $_POST['quote_citationbundle_citationtype'];
            $form->bind($request);
            // on verifie l'image
            if (isset($myfiles))
            {
                $image = $myfiles;
                $imageInfo = getimagesize($image);
                $verif_image = new ImageVerif($image,list($w, $h) = getimagesize($image),$imageInfo);
                $error == null;
            }
            else
            {
                $image = $_FILES['quote_citationbundle_citationtype'];
                $imageInfo = getimagesize($image['tmp_name']['attachment']);
                $verif_image = new ImageVerif($image['tmp_name']['attachment'],$image['size']['attachment'],$imageInfo);
                $error = $verif_image -> verif_image();
            }
            if($form->isValid() && $error == null)
            {
                $nom = $info_cit['auteur'].'-'.$verif_image->random_name();
                $image_file = 'citations-upload/'.$nom.'.png';
                $destination = $DestinationFile.'/'.$nom.'.png';
                $citation->setNomImage($nom);
                if (isset($myfiles))
                    $citation->setUrlImageBase($myfiles);
                else
                    $citation->setUrlImageBase($image['tmp_name']['attachment']);
                if (!$session->get('user_id'))
                    $citation->setUser(13); //Anonymous
                else if ($session->get('admin') == 1)
                    $citation->setUser(0);
                else
                    $citation->setUser($session->get('user_id'));
                $citation->setScore(0);
                $citation->setLangue(1);
                $citation->setDateUpload(new \Datetime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($citation);
                $em->flush();
                if (isset($myfiles))
                    $objetTraitementImage = new ImageTraitement2($image,$destination,$info_cit['citation'],$info_cit['auteur']);
                else
                    $objetTraitementImage = new ImageTraitement($image,$destination,$info_cit['citation'],$info_cit['auteur']);
                $objetTraitementImage -> traitement(); 

                $uploadedfile = $destination;
                $src = imagecreatefrompng($uploadedfile);
                list($width,$height)=getimagesize($uploadedfile);
                $newwidth=450;
                $newheight=600;
                $tmp=imagecreatetruecolor($newwidth,$newheight);
                imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
                $filename = $DestinationMobile.'/'.$nom.'.png';
                Imagepng($tmp,$filename,2);
                imagedestroy($tmp);
                $message_valide = "Merci pour votre contribution.";
            }
        }
        // if (!$session->get('user_id'))
        //     $error_connect = "Vous devez être connecté pour ajouter des citations.<br /><a class='simplelink' href='".$this->generateUrl('quote_citation_connexion')."'>Se connecter</a>"; 
         return $this->render('QuoteCitationBundle:Citation:proposer.html.twig', array(
            'form' => $form->createView(),'message_valide' =>$message_valide,'image_error' => $error,'connect_error' => $error_connect,'image_target' => $image_file
          ));
    }
//----------------------------------------------------------------------------------------ADMIN

//----------------------------------------------------------------------------------------API
    public function apiCitationPrincipalAction($start, $type)
    {
        if ($type == 2)
            $citation = $this->reqSelectCitation($start, 1, 0, "1");
        else
            $citation = $this->reqSelectCitation($start, 1, $type);
        $citation[0]['total'] = $this->apiCitationLimiteAction($type);
        $array = array();
        $array["items"] = $citation; 
        $response = new \Symfony\Component\HttpFoundation\Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function apiCitationSearchAction($start, $recherche)
    {
        $citation = $this->builIndexRechercheAction($start,'Citation',$recherche, 30);
        $array = array();
        $array["items"] = $citation; 
        $response = new \Symfony\Component\HttpFoundation\Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function apiCitationPrincipal2Action($start, $type)
    {
        if ($type == 3) // top
            $citation = $this->reqTopCitation($start, 30);
        else if ($type == 2)
            $citation = $this->reqSelectCitation($start, 30, 0, "1");
        else
            $citation = $this->reqSelectCitation($start, 30, $type);
        $array = array();
        $array["items"] = $citation; 
        $response = new \Symfony\Component\HttpFoundation\Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function apiCitationLimiteAction($type)
    {
        if ($type == 2)
            $nbr = count($this->reqSelectCitation(0, 1000000, 0, "1"));
        else
            $nbr = count($this->reqSelectCitation(0, 1000000, $type));
        return $nbr;
    }

    public function apiCitationCheatAction(Request $request)
    {
        // $session = $request->getSession();
        // if ($session->get('user_id') && $session->get('admin') == 1)
        // {
        //     $citations = $this->reqSelectCitation(0, 1000000, 0);
        //     foreach ($citations as $key => $value) 
        //     {
        //         $nbr = rand(5, 15);
        //         $citationOne = $this->getDoctrine()->getRepository('QuoteCitationBundle:Citation')->find($value['id']);
        //         $citationOne->setScore($nbr);
        //         $em = $this->getDoctrine()->getManager();
        //         $em->persist($citationOne);
        //         $em->flush();
        //     }
        //     return (new \Symfony\Component\HttpFoundation\Response("WORKS !"));
        // }
        return $this->redirect($this->generateUrl('quote_citation_index'));
    }

    public function apiCitationPrincipal3Action($start, $type, $nbr)
    {
        if ($type == 3) // top
            $citation = $this->reqTopCitation($start, $nbr);
        else if ($type == 2)
            $citation = $this->reqSelectCitation($start, $nbr, 0, "1");
        else
            $citation = $this->reqSelectCitation($start, $nbr, $type);
        $array = array();
        $array["items"] = $citation; 
        $response = new \Symfony\Component\HttpFoundation\Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function apiCitationSearch3Action($start, $recherche, $nbr)
    {
        $citation = $this->builIndexRechercheAction($start,'Citation',$recherche, $nbr);
        $array = array();
        $array["items"] = $citation; 
        $response = new \Symfony\Component\HttpFoundation\Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function reqTopCitation($start, $limit)
    {
        if (empty($score))
            $score = self::SCORE;
        $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                'SELECT c
                FROM QuoteCitationBundle:Citation c
                WHERE c.langue = 0
                ORDER BY c.score DESC'
            )
            ->setFirstResult($start)
            ->setMaxResults($limit);
        $result = $query->getResult();
        $citation = array();
        foreach ($result as $value) 
        {
            if (file_exists($this->container->getParameter('kernel.root_dir').'/../web/citations-upload/'.$value->getNomImage().'.png'))
            {
                if ($value->getUser() == 0)
                    $pseudo = "Admin";
                else
                    $pseudo = $this->get_info_user($value->getUser())->getPseudo();
                $citation[] = array(
                    'id' => $value->getId(),
                    'auteur' => $value->getAuteur(), 
                    'imagename' => $value->getNomImage(), 
                    'citation' => strtr($value->getCitation(),array(" " =>"+","%" =>"","." =>"","," =>"",":" =>"","'" =>"")),
                    'pseudo' => $pseudo,
                    'id_user' => $value->getUser(),
                    'score' => $value->getScore()
                    );
            }
        }
        return $citation;
    }

    public function apiCitationPrincipal3RandomAction($langue)
    {
        $citation = $this->reqSelectCitationRandom($langue);
        $array = array();
        $array["items"] = $citation; 
        $response = new \Symfony\Component\HttpFoundation\Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function apiCitationPrincipal3USAAction($start, $type, $nbr)
    {
        if ($type == 3) // top
            $citation = $this->reqTopCitationUSA($start, $nbr);
        else if ($type == 2)
            $citation = $this->reqSelectCitationUSA($start, $nbr, 0, "1");
        else
            $citation = $this->reqSelectCitationUSA($start, $nbr, $type);
        $array = array();
        $array["items"] = $citation; 
        $response = new \Symfony\Component\HttpFoundation\Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function apiCitationSearch3USAAction($start, $recherche, $nbr)
    {
        $citation = $this->builIndexRechercheUSAAction($start,'Citation',$recherche, $nbr);
        $array = array();
        $array["items"] = $citation; 
        $response = new \Symfony\Component\HttpFoundation\Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function reqTopCitationUSA($start, $limit)
    {
        if (empty($score))
            $score = self::SCORE;
        $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                'SELECT c
                FROM QuoteCitationBundle:Citation c
                WHERE c.langue = 1
                ORDER BY c.score DESC'
            )
            ->setFirstResult($start)
            ->setMaxResults($limit);
        $result = $query->getResult();
        $citation = array();
        foreach ($result as $value) 
        {
            if (file_exists($this->container->getParameter('kernel.root_dir').'/../web/citations-upload/'.$value->getNomImage().'.png'))
            {
                if ($value->getUser() == 0)
                    $pseudo = "Admin";
                else
                    $pseudo = $this->get_info_user($value->getUser())->getPseudo();
                $citation[] = array(
                    'id' => $value->getId(),
                    'auteur' => $value->getAuteur(), 
                    'imagename' => $value->getNomImage(), 
                    'citation' => strtr($value->getCitation(),array(" " =>"+","%" =>"","." =>"","," =>"",":" =>"","'" =>"")),
                    'pseudo' => $pseudo,
                    'id_user' => $value->getUser(),
                    'score' => $value->getScore()
                    );
            }
        }
        return $citation;
    }
}
