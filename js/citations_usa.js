$(document).ready(function(){
            window.load = 3;
            window.mytype = 0;
            window.searchclicknbr = 0
            window.starts = 0;
            window.searchpage = 0;
            var FastClick = window.FastClick || { attach : function(){} };          
            FastClick.attach(document.body); 
            function addcontent()
            {
                $('#nomoreresults').hide();
                $('.afficherplususa').hide();
                $('.randomizedusa').hide();
                $('#loading').show();
                var n = 0;
                var response = "";
                var urlImage="http://www.citations.co/citations-mobile/";
                var urlAjax="http://www.citations.co/apiCitation3usa/" + window.starts + "/" + window.mytype + "/" + window.load;
                if (window.mytype == 4)
                            var urlAjax="http://www.citations.co/apisearch2usa/" + window.starts + "/" + window.search + "/" + window.load;
                if (window.mytype == 5)
                            var urlAjax="http://www.citations.co/apiCitation3random/1";
                 $.getJSON(urlAjax,function(json){
                         $.each(json.items,function(i,item){
                            if (item.score > 0)
                                item.score = "+" + item.score;
                            $("#cit").append('<div id="' + item.id + '" class="inlineBlock box">'+
                                 '<img src="' + urlImage + item.imagename + '.png" alt="' + item.auteur + '" class="image-citations">'+
                                 '<div id="vote"><img src="img/votedown.png" class="votedownusa"'+
                                 ' entryid="' + item.id + '">'+
                                '<span class="score" entyid="' + item.id + '">' + item.score + '</span><img src="img/voteup.png"'+
                                'style="position: relative;top: 10px;"'+
                                'class="voteupusa" width="80" style="line-height: 0;" entryid="' + item.id + '"></div></div><hr>');
                             n++;
                         });
                        if(n == 0)
                            $('#nomoreresults').show();
                        else if(window.mytype != 5)
                            $('.afficherplususa').show();
                        else if(window.mytype == 5)
                            $('.randomizedusa').show();
                        $('#loading').hide();
                        window.starts = window.starts + window.load;
                 });
            }
            function remove_class()
            {
                $(".indexusa").removeClass("selected_menu");
                $(".userusa").removeClass("selected_menu");
                $(".contribusa").removeClass("selected_menu");
                $(".topusa").removeClass("selected_menu");
                $(".randomusa").removeClass("selected_menu");
                $(".infousa").removeClass("selected_menu");
            }
            addcontent();
            $(".afficherplususa").bind('click', function()
            {
                addcontent();
                return false;
            });
            
            $(".indexusa").bind('click', function()
            {
                remove_class();
                $(this).addClass( "selected_menu" );
                $('.randomizedusa').hide();
                window.searchpage = 0;
                window.starts = 0;
                $("#cit").empty();
                window.searchclicknbr = 0;
                window.mytype = 0;
                $(".panel").panel("close");
                addcontent();
                return false;
            });

            $(".topusa").bind('click', function()
            {
                remove_class();
                $(this).addClass( "selected_menu" );
                $('.randomizedusa').hide();
                window.searchpage = 0;
                window.starts = 0;
                $("#cit").empty();
                window.searchclicknbr = 0;
                window.mytype = 3;
                $(".panel").panel("close");
                addcontent();
                return false;
            });

            $(".randomusa").bind('click', function()
            {
                if ($(".randomusa").attr('class') == "randomusa")
                {
                    remove_class();
                    $(this).addClass( "selected_menu" );
                }
                $('.afficherplususa').hide();
                window.searchpage = 0;
                window.starts = 0;
                $("#cit").empty();
                window.searchclicknbr = 0;
                window.mytype = 5;
                $(".panel").panel("close");
                addcontent();
                return false;
            });

            $(".infousa").bind('click', function()
            {
                remove_class();
                $(this).addClass( "selected_menu" );
                $('.afficherplususa').hide();
                $('.randomizedusa').hide();
                window.searchpage = 1;
                $('#cit').empty();
                window.searchclicknbr = 0;
                $('#cit').append('<div id="info"><h3>Welcome to the app Citations</h3><hr><p>Browse through quotes and vote for the ones that inspire you! Citations having received a significant number of positive votes will be highlighted in the tabs "Top quotes". </p><hr>This is the first version of the app so do not hesitate to send us your opinions and report any technical issues at this address: citations.co@gmail.com. <br>And if you like this app, don’t forget to grade it on the Play Store<br>Thank you and happy reading! <br>Citations – Inspiration Dealer</div>');
                $(".panel").panel("close");
                return false;
            });

            $(".searchusa").bind('click', function()
            {
                remove_class();
                $('.randomizedusa').hide();
                $('.afficherplususa').hide();
                window.searchpage = 1;
                $('#cit').empty();
                $('#cit').append('<form id="foousa" action="" data-ajax="false"><div data-role="fieldcontain" class="ui-field-contain ui-body ui-br"><label for="search-2" class="ui-input-text">Search:</label><div class="ui-input-search ui-shadow-inset ui-btn-corner-all ui-btn-shadow ui-icon-searchfield ui-body-c"><input type="text" data-type="search" name="search-2" id="search-2" value="" class="ui-input-text ui-body-c"></div></div></form>');
                $(".panel").panel("close");
                window.searchclicknbr++;
                if (window.searchclicknbr > 1)
                {
                    $('#cit').empty();
                    window.searchpage = 0;
                    window.starts = 0;
                    window.searchclicknbr = 0;
                    window.mytype = 0;
                    addcontent();
                    $(".panel").panel("close");
                }
                return false;
            });
            $("#foousa").live("submit", function() 
            {
                window.searchpage = 0;
                window.starts = 0;
                window.mytype = 4;
                window.search = $("#search-2").val();
                if (window.search != "")
                {
                    $('#cit').empty();
                    window.searchclicknbr = 0;
                    addcontent();
                }
                $(".panel").panel("close");
                return false;
            });

            $('.votedownusa').live("click", function(e)
            {

                id = $(this).attr('entryid');
                change = $(this).next();
                change.empty();
                change.append('<img src="img/ajax-loader.gif" alt="loading" class="loader" style="zoom: 1.3;width:32px;-webkit-filter: invert(0%);margin:0;position:relative;top:6px;">');
                $.get("http://www.citations.co/score/" + id +"/0", function(data)
                {   
                    change.empty();
                    change.append(data);
                });
                return false;
             });

            $('.voteupusa').live("click", function(e)
            {
                id = $(this).attr('entryid');
                change = $(this).prev();
                change.empty();
                change.append('<img src="img/ajax-loader.gif" alt="loading" class="loader" style="zoom: 1.3;width:32px;-webkit-filter: invert(0%);margin:0;position:relative;top:6px;">');
                $.get("http://www.citations.co/score/" + id +"/1", function(data)
                {   
                    change.empty();
                    change.append(data);
                });
                return false;
             });
});