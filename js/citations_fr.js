$(document).ready(function(){

            window.load = 3;
            window.mytype = 0;
            window.searchclicknbr = 0
            window.starts = 0;
            window.searchpage = 0;
			var FastClick = window.FastClick || { attach : function(){} };			
			FastClick.attach(document.body); 
            function addcontent()
            {
                $('#nomoreresults').hide();
                $('.afficherplus').hide();
                $('.randomized').hide();
                $('#loading').show();
                var n = 0;
                var response = "";
                var urlImage="http://www.citations.co/citations-mobile/";
                var urlAjax="http://www.citations.co/apiCitation3/" + window.starts + "/" + window.mytype + "/" + window.load;
                if (window.mytype == 4)
                            var urlAjax="http://www.citations.co/apisearch2/" + window.starts + "/" + window.search + "/" + window.load;
                if (window.mytype == 5)
                            var urlAjax="http://www.citations.co/apiCitation3random/0";       
                 $.getJSON(urlAjax,function(json){
                         $.each(json.items,function(i,item){
                            if (item.score > 0)
                                item.score = "+" + item.score;
                            $("#cit").append('<div id="' + item.id + '" class="inlineBlock box">'+
                                 '<img src="' + urlImage + item.imagename + '.png" alt="' + item.auteur + '" class="image-citations">'+
                                 '<div id="vote"><img src="img/votedown.png" class="votedown"'+
                                 ' entryid="' + item.id + '">'+
                                '<span class="score" entyid="' + item.id + '">' + item.score + '</span><img src="img/voteup.png"'+
                                'style="position: relative;top: 10px;"'+
                                'class="voteup" width="80" style="line-height: 0;" entryid="' + item.id + '"></div></div><hr>');
                             n++;
                         });
                        if(n == 0)
                            $('#nomoreresults').show();
                        else if(window.mytype != 5)
                            $('.afficherplus').show();
                        else if(window.mytype == 5)
                            $('.randomized').show();
                        $('#loading').hide();
                        window.starts = window.starts + window.load;
                 });
            }
            function remove_class()
            {
                $(".index").removeClass("selected_menu");
                $(".user").removeClass("selected_menu");
                $(".contrib").removeClass("selected_menu");
                $(".top").removeClass("selected_menu");
                $(".random").removeClass("selected_menu");
                $(".info").removeClass("selected_menu");
            }

            addcontent();
            $(".afficherplus").bind('click', function()
            {
                addcontent();
                return false;
            });
			
            $(".index").bind('click', function()
            {
                remove_class();
                $(this).addClass( "selected_menu" );
            	window.searchpage = 0;
            	window.starts = 0;
            	$("#cit").empty();
            	window.searchclicknbr = 0;
				window.mytype = 0;
				$(".panel").panel("close");
				addcontent();
				return false;
            });

            $(".user").bind('click', function()
            {
                remove_class();
                $(this).addClass( "selected_menu" );
                $('.randomized').hide();
            	window.searchpage = 0;
            	window.starts = 0;
            	$("#cit").empty();
            	window.searchclicknbr = 0;
				window.mytype = 1;
				$(".panel").panel("close");
				addcontent();
				return false;
            });
            $(".contrib").bind('click', function()
            {
                remove_class();
                $(this).addClass( "selected_menu" );
                $('.randomized').hide();
            	window.searchpage = 0;
            	window.starts = 0;
            	$("#cit").empty();
            	window.searchclicknbr = 0;
				window.mytype = 2;
				$(".panel").panel("close");
				addcontent();
				return false;
            });
            $(".top").bind('click', function()
            {
                remove_class();
                $(this).addClass( "selected_menu" );
                $('.randomized').hide();
            	window.searchpage = 0;
            	window.starts = 0;
            	$("#cit").empty();
            	window.searchclicknbr = 0;
				window.mytype = 3;
				$(".panel").panel("close");
				addcontent();
				return false;
            });

            $(".random").bind('click', function()
            {
                if ($(".random").attr('class') == "random")
                {
                    remove_class();
                    $(this).addClass( "selected_menu" );
                }
                $('.afficherplus').hide();
                window.searchpage = 0;
                window.starts = 0;
                $("#cit").empty();
                window.searchclicknbr = 0;
                window.mytype = 5;
                $(".panel").panel("close");
                addcontent();
                return false;
            });

            $(".info").bind('click', function()
            {
                remove_class();
                $(this).addClass( "selected_menu" );
                $('.randomized').hide();
            	$('.afficherplus').hide();
            	window.searchpage = 1;
            	$('#cit').empty();
            	window.searchclicknbr = 0;
            	$('#cit').append('<div id="info"><h3>Bienvenue sur l\'application officielle de citations.co</h3><hr><p>Vous pouvez proposer en quelques cliques des citations personnelles ou célèbres sur notre site web citations.co.<br> Toutes vos citations apparaîtront dans l\'onglet "Contributions" ! Les citations ayant reçu un nombre important de vote positif seront mises en avant dans les onglets "Citations célèbre" et "Citations d\'utilisateurs" en fonction de la catégorie que vous avez sélectionnée.</p><hr>C\'est la première version de Citations alors n\'hésitez pas à nous envoyer vos avis et les éventuels bugs de l\'application à cette adresse:citations.co@gmail.com<br>Et si vous aimez cette application, n\'oubliez pas la noter sur le Playstore!<br>Merci et bonne lecture !<br>Citations.co - Dealer d\'inspiration</div>');
				$(".panel").panel("close");
				return false;
            });

            $(".search").bind('click', function()
            {
                remove_class();
                $('.randomized').hide();
            	$('.afficherplus').hide();
            	window.searchpage = 1;
            	$('#cit').empty();
            	$('#cit').append('<form id="foo" action="" data-ajax="false"><div data-role="fieldcontain" class="ui-field-contain ui-body ui-br"><label for="search-2" class="ui-input-text">Recherche:</label><div class="ui-input-search ui-shadow-inset ui-btn-corner-all ui-btn-shadow ui-icon-searchfield ui-body-c"><input type="text" data-type="search" name="search-2" id="search-2" value="" class="ui-input-text ui-body-c"></div></div></form>');
				$(".panel").panel("close");
				window.searchclicknbr++;
				if (window.searchclicknbr > 1)
				{
					$('#cit').empty();
					window.searchpage = 0;
					window.starts = 0;
					window.searchclicknbr = 0;
					window.mytype = 0;
					addcontent();
					$(".panel").panel("close");
				}
				return false;
            });
			$("#foo").live("submit", function() 
			{
				window.searchpage = 0;
            	window.starts = 0;
				window.mytype = 4;
				window.search = $("#search-2").val();
				if (window.search != "")
				{
					$('#cit').empty();
					window.searchclicknbr = 0;
					addcontent();
				}
				$(".panel").panel("close");
				return false;
			});

            $('.votedown').live("click", function(e)
            {

				id = $(this).attr('entryid');
				change = $(this).next();
				change.empty();
				change.append('<img src="img/ajax-loader.gif" alt="loading" class="loader" style="zoom: 1.3;width:32px;-webkit-filter: invert(0%);margin:0;position:relative;top:6px;">');
				$.get("http://www.citations.co/score/" + id +"/0", function(data)
				{	
					change.empty();
					change.append(data);
				});
				return false;
           	 });

            $('.voteup').live("click", function(e)
            {
				id = $(this).attr('entryid');
				change = $(this).prev();
				change.empty();
				change.append('<img src="img/ajax-loader.gif" alt="loading" class="loader" style="zoom: 1.3;width:32px;-webkit-filter: invert(0%);margin:0;position:relative;top:6px;">');
				$.get("http://www.citations.co/score/" + id +"/1", function(data)
				{	
					change.empty();
					change.append(data);
				});
				return false;
           	 });
});